import serial
import modulePompe as pmp
import time
import winsound as ws
import numpy as np
from matplotlib import pyplot as plt

pump = pmp.Pump(port='COM3',
    			baudrate=9600,
			    parity=serial.PARITY_NONE,
			    stopbits=serial.STOPBITS_ONE,
			    bytesize=serial.EIGHTBITS,
			    timeout=1)

rateb = 10 #float(raw_input("Vitesse initiale?"))
ratef = 1000 #float(raw_input("Vitesse finale?"))
duree = 50 #float(raw_input("Duree?"))
print("la rampe d'injection est configuree pour aller de "+str(rateb)+" ul/m a "+str(ratef)+" ul/m en "+str(duree)+" secondes")
rate = float(rateb) # flux a l'instant t, pour t=0 c'est la vitesse initiale
steptime = float(raw_input("Choisissez l'intervalle de temps entre chaque augmentation de la vitesse d'injection >> ")) # temps en secondes a attendre entre chaque augmentation du flux, depend du temps que mettent les images a se stocker
rateInstantT = []
slope = (ratef - rateb) / (duree) #coefficient directeur de la droite de rate en fonction du temps

#print('serial link open ?' , pump.isOpen())  #Teste si le port est ouvert
#raw_input('continue ?')



# ----- INITIALISATION DE LA POMPE ------ #

pump.setrate(rateb) #initialising the pump
time.sleep(2)
pump.run()
ti = time.time() # save the time of beginning
tfraction = ti
vfraction = 0
delay = 4 #s

# initialisation
allrates=[]
allbeeptimes=[]
allvolumes=[]
# -------------- BOUCLE ------------------#
# condInit = True

while time.time() <= ti + duree:

	rate = float(format((slope * (time.time() - ti) + rateb), '2.1f'))
	print(str(rate))
	rateInstantT.append( str("la vitesse au temps " + str(time.time()-ti) + " vaut " + str(rate)) )
	pump.setrate(rate)

	#if(float(rate)*(time.time()-tfraction) > float(20)):
	#	ws.Beep(440,300)
	#	tfraction = 

	vfraction += rate*((time.time()-tfraction)/60)
	if vfraction >= 20:
		ws.Beep(400+int(rate/2),50)
		allrates.append(rate)
		allbeeptimes.append(time.time())
		allvolumes.append(vfraction)
		vfraction = 0
		tfraction = time.time()

	time.sleep(steptime )

# after the end of loop
time.sleep(delay)
pump.stop()

#put data into single array 
alldata = np.array([allbeeptimes,allrates,allvolumes])
print(alldata)
#save in file 
np.savetxt('testgradient.dat',alldata.T)

# set time relative to begining
starttime = np.array([ti, 0., 0.]).reshape(3,1)
alldata   = alldata - starttime
print(alldata)

#draw 2 figures
plt.plot(alldata[0],alldata[1]
	)
plt.title('beeptimes rates')
plt.show()

plt.plot(alldata[0],alldata[2])
plt.title('beeptimes volumes')
plt.show()

