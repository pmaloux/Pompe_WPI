import serial
import modulePompe as pmp
import time
import winsound as ws


pump = pmp.Pump(port='COM3',
    			baudrate=9600,
			    parity=serial.PARITY_NONE,
			    stopbits=serial.STOPBITS_ONE,
			    bytesize=serial.EIGHTBITS,
			    timeout=1)

rateb = 200 #float(raw_input("Vitesse initiale?"))
ratef = 1000 #float(raw_input("Vitesse finale?"))
duree = 50 #float(raw_input("Duree?"))
print("la rampe d'injection est configuree pour aller de "+str(rateb)+" ul/m a "+str(ratef)+" ul/m en "+str(duree)+" secondes")
rate = rateb # flux a l'instant t, pour t=0 c'est la vitesse initiale
#steptime = float(raw_input("Choisissez l'intervalle de temps entre chaque augmentation de la vitesse d'injection >> ")) # temps en secondes a attendre entre chaque augmentation du flux, depend du temps que mettent les images a se stocker
rateInstantT = []
slope = (ratef - rateb) / (duree) #coefficient directeur de la droite de rate en fonction du temps
resolution = raw_input("Choisissez la resolution de la rampe ") #laresolution est la valeur en ul/m que l'on ajoute apres chue remplissage d'un puit


#print('serial link open ?' , pump.isOpen())
#raw_input('continue ?')





# ----- INITIALISATION DE LA POMPE ------ #

pump.setrate(rateb) #initialising the pump
time.sleep(1)
pump.run()
ti = time.time() # save the time of beginning
tfraction = ti


# -------------- BOUCLE ------------------#


time.sleep(0.01)
print(time.time()-ti)
print(str(rate))

while rate <= ratef :


	if ((float(rate)*(time.time()-tfraction)/60) > 20.0):
		ws.Beep(440,300)
		rate += float(resolution)
		print(str(rate))
		rateInstantT.append( str("la vitesse au temps " + str(time.time()-ti) + " vaut " + str(rate)) )
		pump.setrate(rate)
		tfraction = time.time()


	else:
		time.sleep(1)


pump.stop()



