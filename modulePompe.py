#########################################################
#			           MODULE POMPE					    #
#########################################################


import serial
import time

#------------------- ---- CLASSES ------------------------#


class Pump(serial.Serial):




#---------------------- METHODES ------------------------#

	def run(self):
		self.write("run".encode('ascii') +"\r\n")



	def setmode(self, n):  # Definit le mode de la pompe, soit i, soit w
		
		if n == i :
			cdm = str("mode "+ str(n))
			ser.write(cmd.encode('ascii')+'\r\n')
			print("Vous avez choisis le mode Infusion")
			return 1

		elif n == w :
			cdm = str("mode "+ str(n))
			ser.write(cmd.encode('ascii')+'\r\n')
			print("Vous avez choisis le mode Withdrawal")
			return 2

		else :
			print("Argument invalide, entrez soit i pour infusion soit w pour pompage")
			return 0



	def mode(self):  # Demande le mode de la pompe, i ou w
		
		cdm = "mode?"
		self.write(cmd.encode('ascii')+'\r\n')




	def setrate(self, rate):  # Definit la vitesse de la pompe selon son mode, i ou w

		cmd = str("ratei "+str(rate)+ " ul/m")
		self.write(cmd.encode('ascii')+'\r\n')
		#print("La vitesse d'infusion est de "+str(n)+" ul/m")



	def stop(self):  # Stoppe la pompe
		
		cmd = "stop"
		self.write(cmd.encode('ascii')+'\r\n')


# for tests
if __name__ == '__main__':

	cmd = ''

	print("Tests in Module")
	pump = Pump(port='COM3',
    			baudrate=9600,
			    parity=serial.PARITY_NONE,
			    stopbits=serial.STOPBITS_ONE,
			    bytesize=serial.EIGHTBITS,
			    timeout=1
			    )
